﻿using MongoDB.DataModel;
using MongoDB.UIController;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MongoDB
{
    public partial class TeamMemberForm : Form
    {
        private ProjectControllerUI pController;
        private OrganizationalUnit ou;
        public TeamMemberForm(OrganizationalUnit _ou)
        {
            InitializeComponent();
            ou = _ou;

            pController = new ProjectControllerUI();
        }

        private void TeamMemberForm_Load(object sender, EventArgs e)
        {

            pController.getAllTeamMember(dataGridView1, ou);
            AdjustColumnOrder();
        }

        private void AdjustColumnOrder()
        {
            dataGridView1.Columns["PersonID"].DisplayIndex = 0;
            dataGridView1.Columns["FirstName"].DisplayIndex = 1;
            dataGridView1.Columns["LastNAme"].DisplayIndex = 2;
            dataGridView1.Columns["WorkInDepartmantName"].DisplayIndex = 3;
            dataGridView1.Columns["LeaderOfDepartmant"].DisplayIndex = 4;
        }
    }
}
