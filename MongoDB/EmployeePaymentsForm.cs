﻿using MongoDB.DataModel;
using MongoDB.UIController;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MongoDB
{
    public partial class EmployeePaymentsForm : Form
    {
        private WorkersControllerUI wController;
        private PaymentControllerUI pController;
        private Employee employee;
        private List<Payment> payments;

        public EmployeePaymentsForm(Employee _employee)
        {
            InitializeComponent();
            wController = new WorkersControllerUI();
            pController = new PaymentControllerUI();
            employee = _employee;

            dataGridView1.ContextMenuStrip = cms;
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MyDataGridView_MouseDown);
        }

        private void MyDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridView1.HitTest(e.X, e.Y);
                dataGridView1.ClearSelection();
                dataGridView1.Rows[hti.RowIndex].Selected = true;
            }
        }

        private void EmployeePaymentsForm_Load(object sender, EventArgs e)
        {
            refreshForm();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Int32 rowToDelete = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            Payment payment = new Payment();
            payment = (DataModel.Payment)dataGridView1.Rows[rowToDelete].DataBoundItem;
            pController.removePayment(payment, employee);

            refreshForm();
        }

        private void refreshForm()
        {
            payments = new List<Payment>();
            employee = wController.getEmployeeById(employee.Id.ToString());
            foreach (var item in employee.ListOfPayment)
            {
                payments.Add(item);
            }

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = payments;
        }

        
    }
}
