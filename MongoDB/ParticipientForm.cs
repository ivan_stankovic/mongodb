﻿using MongoDB.Bson;
using MongoDB.DataModel;
using MongoDB.Driver;
using MongoDB.UIController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace MongoDB
{
    public partial class ParticipientForm : Form
    {
        private TaskControllerUI tController;
        private List<MongoDBRef> person;
        private TaskForm tf;

        public ParticipientForm(TaskForm _tf)
        {
            InitializeComponent();
            tController = new TaskControllerUI();
            tController.showEmployee(cbPersonID);
            person = new List<MongoDBRef>();

            tf = _tf;

            person = tf.person;

            

            dataGridView1.ContextMenuStrip = cms;
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MyDataGridView_MouseDown);
        }

        private void MyDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridView1.HitTest(e.X, e.Y);
                dataGridView1.ClearSelection();
                dataGridView1.Rows[hti.RowIndex].Selected = true;
            }
        }

        private void ParticipientForm_Load(object sender, EventArgs e)
        {
            refreshForm(person);
        }

        private void cbPersonID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbPersonID.SelectedIndex != -1)
            {
                txtPerson.Text = (cbPersonID.SelectedItem as Employee).FirstName + " " + (cbPersonID.SelectedItem as Employee).LastName;
            }
        }

        private void btnPlus_Click(object sender, EventArgs e)
        {
            List<Employee> employee = tController.showAllEmloyee();

            Employee p = new Employee();
            if (cbPersonID.SelectedIndex != -1)
            {
                p.PersonID = (cbPersonID.SelectedItem as Employee).PersonID;
                foreach (var item in employee)
                {
                    if (item.PersonID == p.PersonID)
                    {
                        ObjectId employeId = item.Id;
                        var employeeRef = new MongoDBRef("employee_id", employeId);
                        person.Add(employeeRef);
                    }
                }
            }

            refreshForm(person);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            tf.listView1.Items.Clear();
            foreach (var item in person)
            {
                Employee employe = tController.getEmployeeById(item.Id.ToString());
                ListViewItem lvi = new ListViewItem(employe.FirstName + " " + employe.LastName);
                tf.listView1.BeginUpdate();
                tf.listView1.Items.Add(lvi);
                tf.listView1.EndUpdate();
            }

            tf.person = person;
            this.Close();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Int32 rowToDelete = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            Employee employee = (Employee)dataGridView1.Rows[rowToDelete].DataBoundItem;
            ObjectId employeId = employee.Id;
            var employeeRef = new MongoDBRef("employee_id", employeId);
            person.Remove(employeeRef);

            refreshForm(person);
        }

        private void refreshForm(List<MongoDBRef> persons)
        {
            List<Employee> emlpoyee = new List<Employee>();
            foreach (var item in person)
            {
               Employee e =   tController.getEmployeeById(item.Id.ToString());
               emlpoyee.Add(e);
            }

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = emlpoyee;
        }
    }
}