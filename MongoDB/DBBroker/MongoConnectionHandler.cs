﻿using MongoDB.DataModel;
using MongoDB.Driver;

namespace MongoDB.DBBroker
{
    internal class MongoConnectionHandler<T> where T : IMongoEntity
    {
        public MongoCollection<T> MongoCollection { get; private set; }
     
        private MongoDatabase db;

        public MongoConnectionHandler()
        {
            const string connectionString = "mongodb://localhost";
            var mongoClient = new MongoClient(connectionString);
            var mongoServer = mongoClient.GetServer();
            const string databaseName = "ncmaster";
            db = mongoServer.GetDatabase(databaseName);
            MongoCollection = db.GetCollection<T>(typeof(T).Name.ToLower());
        }
    }
}