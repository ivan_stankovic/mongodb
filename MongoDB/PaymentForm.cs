﻿using MongoDB.Bson;
using MongoDB.DataModel;
using MongoDB.Helper;
using MongoDB.UIController;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MongoDB
{
    public partial class PaymentForm : Form
    {
        private PaymentControllerUI pController;

        public PaymentForm()
        {
            InitializeComponent();
            
            pController = new PaymentControllerUI();
            pController.showEmployee(cbPerson);

            pController.showPaymentBasis(cbBasis);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
           if (Validation.IsStringEmpty(txtPayment) &&
                Validation.IsNumber(txtPayment) &&
                Validation.IsSelectedItem(cbPerson, "employee") &&
                Validation.IsSelectedItem(cbBasis, "basis"))
            {
                double basis = Convert.ToDouble((cbBasis.SelectedItem as PaymentBasis).Basis);
                double payout = Convert.ToDouble(txtPayment.Text);
                if (payout >= basis)
                {
                            pController.addNewPayment(txtPayment, txtPerson, dateTimePicker1, cbPerson, cbBasis);
                            cleanFields();
                }
                else
                {
                    txtPayment.Focus();
                    MessageBox.Show("Payment must be greater than or equal basis", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
                 
        }

        private void PaymentForm_Load(object sender, EventArgs e)
        {
            cleanFields();
        }

        private void cbPerson_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbPerson.SelectedIndex != -1)
            {
                txtPerson.Text = (cbPerson.SelectedItem as Employee).FirstName + " " + (cbPerson.SelectedItem as Employee).LastName;
            }
        }

        public void cleanFields()
        {
            txtPerson.Text = "";
            txtPayment.Text = "";
            cbBasis.SelectedIndex = -1;
            cbPerson.SelectedIndex = -1;
            dateTimePicker1.Value = DateTime.Now;
        }

    }
}
