﻿using MongoDB.Bson;
using MongoDB.DataModel;
using MongoDB.Driver;
using MongoDB.Helper;
using MongoDB.UIController;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MongoDB
{
    public partial class TaskForm : Form
    {
        private TaskControllerUI tController;
        public List<MongoDBRef> person;
        private ObjectId ID;

        public TaskForm()
        {
            InitializeComponent();
            tController = new TaskControllerUI();

            person = new List<MongoDBRef>();

            tController.showProject(cbProject);
            tController.showTypeOfTask(cbType);

            dataGridView1.ContextMenuStrip = cms;
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MyDataGridView_MouseDown);

        }

        private void MyDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridView1.HitTest(e.X, e.Y);
                dataGridView1.ClearSelection();
                dataGridView1.Rows[hti.RowIndex].Selected = true;
            }
        }

        private void refreshTask()
        {
            tController.showAllTask(dataGridView1);
        }

        private void cleanFields()
        {
            txtName.Text = "";
            cbType.SelectedIndex = -1;
            cbProject.SelectedIndex = -1;
            taskDate.Value = DateTime.Now;
            planedDate.Value = DateTime.Now;
            actualDate.Value = DateTime.Now;
            listView1.Items.Clear();
            btnEdit.Enabled = false;
            btnAdd.Enabled = true;

        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {

            btnEdit.Enabled = true;
            btnAdd.Enabled = false;
            person.Clear();
            listView1.Items.Clear();
            Int32 rowToUpdate = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataModel.Task t = new DataModel.Task();
            t = (DataModel.Task)dataGridView1.Rows[rowToUpdate].DataBoundItem;
            t = tController.getById(t);
            ID = t.Id;
            txtName.Text = t.TaskName;
            var v = t.Project.Id;
            Project p =  tController.getProjectById(t.Project.Id.ToString());
            cbProject.SelectedIndex = cbProject.FindStringExact(p.ProjectName);
            TypeOfTasks tot = tController.getTypeOfTaskById(t.TypesOfTask.Id.ToString());
            cbType.SelectedIndex = cbType.FindStringExact(tot.TypeName);

            taskDate.Value = Convert.ToDateTime(t.TaskDate);
            planedDate.Value = Convert.ToDateTime(t.PlannedEndDate);
            actualDate.Value = Convert.ToDateTime(t.ActualEndDate);
            if (t.Participants.Count != 0)
            {
                for (int i = 0; i < t.Participants.Count; i++)
                {
                    person.Add(t.Participants[i]);
                    Employee employee = tController.getEmployeeById(t.Participants[i].Id.ToString());
                    ListViewItem lvi = new ListViewItem(employee.FirstName+" "+employee.LastName);
                    listView1.BeginUpdate();
                    listView1.Items.Add(lvi);
                    listView1.EndUpdate();
                }
            }

            ParticipientForm pf = new ParticipientForm(this);

            refreshTask();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Int32 rowToDelete = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataModel.Task t = new DataModel.Task();
            t = (DataModel.Task)dataGridView1.Rows[rowToDelete].DataBoundItem;
            tController.removeTask(t);

            refreshTask();
        }

        private void btnPlus_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            ParticipientForm pf = new ParticipientForm(this);
            pf.ShowDialog();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (Validation.IsStringEmpty(txtName) &&
                Validation.IsSelectedItem(cbProject, "project") &&
                Validation.IsSelectedItem(cbType, "type of task"))
            {
                tController.addNewTask(txtName, cbProject, cbType, taskDate, planedDate, actualDate, person);
                cleanFields();

                refreshTask();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (Validation.IsStringEmpty(txtName) &&
                Validation.IsSelectedItem(cbProject, "project") &&
                Validation.IsSelectedItem(cbType, "type of task"))
            {
                tController.editTask(ID, txtName, cbProject, cbType, taskDate, planedDate, actualDate, person);
                cleanFields();

                refreshTask();
            }
        }

        private void TaskForm_Load(object sender, EventArgs e)
        {
            refreshTask();
            cleanFields();
        }

        private void participientsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            person.Clear();
            Int32 rowToUpdate = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataModel.Task t = new DataModel.Task();
            t = (DataModel.Task)dataGridView1.Rows[rowToUpdate].DataBoundItem;
            t = tController.getById(t);
            ID = t.Id;
            txtName.Text = t.TaskName;
            Project p = tController.getProjectById(t.Project.Id.ToString());
            cbProject.SelectedIndex = cbProject.FindStringExact(p.ProjectName);
            TypeOfTasks tots = tController.getTypeOfTaskById(t.TypesOfTask.Id.ToString());
            cbType.SelectedIndex = cbType.FindStringExact(tots.TypeName);

            taskDate.Value = Convert.ToDateTime(t.TaskDate);
            planedDate.Value = Convert.ToDateTime(t.PlannedEndDate);
            actualDate.Value = Convert.ToDateTime(t.ActualEndDate);
            if (t.Participants.Count != 0)
            {
                for (int i = 0; i < t.Participants.Count; i++)
                {
                    person.Add(t.Participants[i]);
                }
            }

            TaskMemberForm tmf = new TaskMemberForm(this);
            tmf.Show();
        }

    }
}
