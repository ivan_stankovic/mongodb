﻿using System;
using System.Windows.Forms;

namespace MongoDB
{
    //public class Isplata
    //{
    //    public ObjectId isplata_id { get; set; }
    //    public string datum { get; set; }
    //    public int iznos { get; set; }
    //    public int osnova { get; set; }
    //}

    //public class RootObject
    //{
    //    [BsonId]
    //    public string _id { get; set; }
    //    public string id { get; set; }
    //    public string jmbg { get; set; }
    //    public string ime { get; set; }
    //    public string prezime { get; set; }
    //    public List<Isplata> isplata { get; set; }
    //    public string radi { get; set; }
    //    public string orgjed_id { get; set; }
    //    public bool rukovodi { get; set; }
    //}

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //var connectionString = "mongodb://localhost";
            //var client = new MongoClient(connectionString);
            //var server = client.GetServer();
            //var database = server.GetDatabase("ncmaster");
            //var collection = database.GetCollection<RootObject>("zaposleni");

            //var entity = new RootObject { _id = ObjectId.GenerateNewId().ToString() , id="123", ime="BLA", jmbg="12345667", isplata = new List<Isplata>(), orgjed_id="123213", prezime="BLA", radi="Test", rukovodi=true};
            //collection.Insert(entity);
        }

        private void btnCoWorkers_Click(object sender, EventArgs e)
        {
            CoworkerForm cf = new CoworkerForm();
            cf.ShowDialog();
        }

        private void btnProject_Click(object sender, EventArgs e)
        {
            ProjectForm pf = new ProjectForm();
            pf.ShowDialog();
        }

        private void btnTypeOfTasks_Click(object sender, EventArgs e)
        {
            TypeOfTaskForms totf = new TypeOfTaskForms();
            totf.ShowDialog();
        }

        private void btnPaymentBasis_Click(object sender, EventArgs e)
        {
            PaymentBasisForm pbf = new PaymentBasisForm();
            pbf.ShowDialog();
        }

        private void btnEmployee_Click(object sender, EventArgs e)
        {
            EmployeeForm ef = new EmployeeForm();
            ef.ShowDialog();
        }

        private void btnPayment_Click(object sender, EventArgs e)
        {
            PaymentForm pf = new PaymentForm();
            pf.ShowDialog();
        }

        private void btnOrganizationUnit_Click(object sender, EventArgs e)
        {
            OrganizationUnitForm ouf = new OrganizationUnitForm();
            ouf.ShowDialog();
        }

        private void btnTasks_Click(object sender, EventArgs e)
        {
            TaskForm tf = new TaskForm();
            tf.ShowDialog();
        }
    }
}