﻿using MongoDB.Bson;
using MongoDB.DataModel;
using MongoDB.Service;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MongoDB.UIController
{
    internal class PaymentControllerUI
    {
        #region PaymentBasis

        private PaymentBasisService pbs = new PaymentBasisService();

        public void addNewPaymentBasis(TextBox txtBasis)
        {
            double basis = Convert.ToDouble(txtBasis.Text);

            PaymentBasis pBasis = new PaymentBasis();
            pBasis.Basis = basis;

            pbs.Create(pBasis);
        }

        public void removePaymentBasis(PaymentBasis basis)
        {
            pbs.Delete(basis.Id.ToString());
        }

        public void showAllPaymetBasis(DataGridView dgv)
        {
            dgv.DataSource = null;
            dgv.DataSource = pbs.GetAll();
        }

        public void showPaymentBasis(ComboBox cmb)
        {
            cmb.DataSource = null;
            cmb.DataSource = pbs.GetAll();
        }

        #endregion PaymentBasis

        #region Payment

        private PaymentService ps = new PaymentService();
        private EmployeeService es = new EmployeeService();

        public void showEmployee(ComboBox cmb)
        {
            cmb.DataSource = null;
            cmb.DataSource = es.GetAll();
        }

        public void addNewPayment(TextBox txtPayment, TextBox txtPerson, DateTimePicker date, ComboBox cbPersonID, ComboBox cbPaymentBasis)
        {
            double payout = Convert.ToDouble(txtPayment.Text);

            Payment p = new Payment();
            p.Id = ObjectId.GenerateNewId();
            p.Payout = payout;
            p.PaymentDate = String.Format("{0:s}", date.Value);
            p.PaymentBasis = (cbPaymentBasis.SelectedItem as PaymentBasis);
            Employee e =  es.GetById((cbPersonID.SelectedItem as Employee).Id.ToString());
            if (e.ListOfPayment == null)
            {
                e.ListOfPayment = new System.Collections.Generic.List<Payment>();
            }
            e.ListOfPayment.Add(p);
            es.Update(e);
        }

        public void editPayment(ObjectId ID, TextBox txtPayment, TextBox txtPerson, DateTimePicker date, ComboBox cbPersonID, ComboBox cbPaymentBasis)
        {
            //double payout = Convert.ToDouble(txtPayment.Text);

            //Payment p = new Payment();
            //p.Id = ID;
            //p.Employee = (cbPersonID.SelectedItem as Employee);
            //p.Payout = payout;
            //p.PaymentDate = String.Format("{0:s}", date.Value);
            //p.PaymentBasis = (cbPaymentBasis.SelectedItem as PaymentBasis).Basis;

            //ps.Update(p);
        }

        public void removePayment(Payment payment, Employee employee)
        {
            List<Payment> payouts = new List<Payment>();

            if (employee.ListOfPayment == null)
            {
                employee.ListOfPayment = new System.Collections.Generic.List<Payment>();
            }
            else
            {
                foreach (var item in employee.ListOfPayment)
                {
                    if (payment.Id != item.Id)
                    {
                        payouts.Add(item);
                    }
                }

                employee.ListOfPayment = payouts;
            }

            es.Update(employee);
        }

        #endregion Payment
    }
}