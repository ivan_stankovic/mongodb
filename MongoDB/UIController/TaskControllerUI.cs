﻿using MongoDB.Bson;
using MongoDB.DataModel;
using MongoDB.Driver;
using MongoDB.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MongoDB.UIController
{
    internal class TaskControllerUI
    {
        #region Type of tasks

        private TypeOfTaskService tots = new TypeOfTaskService();

        public void addNewTypeOfTask(TextBox txtType)
        {
            string type = txtType.Text;

            TypeOfTasks typeOfTask = new TypeOfTasks();
            typeOfTask.TypeName = type;

            tots.Create(typeOfTask);
        }

        public void removeTypeOfTask(TypeOfTasks type)
        {
            tots.Delete(type.Id.ToString());
        }

        public void showAllTypesOfTasks(DataGridView dgv)
        {
            dgv.DataSource = null;
            dgv.DataSource = tots.GetAll();
        }

        #endregion Type of tasks

        private TaskService ts = new TaskService();
        private ProjectService ps = new ProjectService();
        private EmployeeService es = new EmployeeService();

        public void addNewTask(TextBox txtName, ComboBox cbProject, ComboBox cbType, DateTimePicker dateTask, DateTimePicker planDate, DateTimePicker actualDate, List<MongoDBRef> participient)
        {
            DataModel.Task t = new DataModel.Task();
            t.TaskName = txtName.Text;
            ObjectId projectId = (cbProject.SelectedItem as Project).Id;
            t.Project = new MongoDBRef("project_id", projectId);
            ObjectId typeId = (cbType.SelectedItem as TypeOfTasks).Id;
            t.TypesOfTask = new MongoDBRef("type_id", typeId);
            t.TaskDate = String.Format("{0:s}", dateTask.Value);
            t.PlannedEndDate = String.Format("{0:s}", planDate.Value);
            t.ActualEndDate = String.Format("{0:s}", actualDate.Value);
            t.Participants = participient;

            ts.Create(t);
        }

        public void editTask(ObjectId ID, TextBox txtName, ComboBox cbProject, ComboBox cbType, DateTimePicker dateTask, DateTimePicker planDate, DateTimePicker actualDate, List<MongoDBRef> participient)
        {
            DataModel.Task t = new DataModel.Task();
            t.Id = ID;
            t.TaskName = txtName.Text; 
            ObjectId projectId = (cbProject.SelectedItem as Project).Id;
            t.Project = new MongoDBRef("project_id", projectId);
            ObjectId typeId = (cbType.SelectedItem as TypeOfTasks).Id;
            t.TypesOfTask = new MongoDBRef("type_id", typeId);
            t.TaskDate = String.Format("{0:s}", dateTask.Value);
            t.PlannedEndDate = String.Format("{0:s}", planDate.Value);
            t.ActualEndDate = String.Format("{0:s}", actualDate.Value);
            t.Participants = participient;

            ts.Update(t);
        }

        private string people(Dictionary<string, string> participient)
        {
            StringBuilder sp = new StringBuilder();
            if (participient.Count != 0)
            {
                for (int i = 0; i < participient.Count; i++)
                {
                    var key = (from a in (participient.Select(b => b.Key).AsEnumerable())
                               select a).ToList();
                    var value = (from a in (participient.Select(b => b.Value).AsEnumerable())
                                 select a).ToList();
                    sp.Append("'");
                    sp.Append(key[i]);
                    sp.Append("':'");
                    sp.Append(value[i]);
                    sp.Append("'");
                    if (i < participient.Count - 1)
                        sp.Append(",");
                }
            }
            return sp.ToString();
        }

        public DataModel.Task getById(DataModel.Task task)
        {
            return ts.GetById(task.Id.ToString());
        }

        public Project getProjectById(string id)
        {
            return ps.GetById(id);
        }

        public Employee getEmployeeById(string id)
        {
            return es.GetById(id);
        }

        public TypeOfTasks getTypeOfTaskById(string id)
        {
            return tots.GetById(id);
        }

        public void removeTask(DataModel.Task task)
        {
            ts.Delete(task.Id.ToString());
        }

        public void showAllTask(DataGridView dgv)
        {
            dgv.DataSource = null;
            dgv.DataSource = ts.GetAll();
        }

        public void showEmployee(ComboBox cmb)
        {
            cmb.DataSource = null;
            cmb.DataSource = es.GetAll();
        }

        public List<Employee> showAllEmloyee()
        {
            return es.GetAll();
        }

        public void showProject(ComboBox cmb)
        {
            cmb.DataSource = null;
            cmb.DataSource = ps.GetAll();
        }

        public void showTypeOfTask(ComboBox cmb)
        {
            cmb.DataSource = null;
            cmb.DataSource = tots.GetAll();
        }
    }
}