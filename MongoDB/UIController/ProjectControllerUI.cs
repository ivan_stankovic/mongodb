﻿using MongoDB.Bson;
using MongoDB.DataModel;
using MongoDB.Driver;
using MongoDB.Service;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace MongoDB.UIController
{
    internal class ProjectControllerUI
    {
        #region Project

        private ProjectService ps = new ProjectService();

        public void addNewProject(TextBox txtProjectName)
        {
            string projectName = txtProjectName.Text;

            Project project = new Project();
            project.ProjectName = projectName;

            ps.Create(project);
        }

        public void removeProject(Project project)
        {
            ps.Delete(project.Id.ToString());
        }

        public void showAllProject(DataGridView dgv)
        {
            dgv.DataSource = null;
            dgv.DataSource = ps.GetAll();
        }

        #endregion Project

        #region Organization Unit

        private OrganizationUnitService ous = new OrganizationUnitService();
        private EmployeeService es = new EmployeeService();

        public void showEmployee(ComboBox cmb)
        {
            cmb.DataSource = null;
            cmb.DataSource = es.GetAll();
        }

        public Employee getEmployeeById(string id)
        {
            return es.GetById(id);
        }

        public void addNewOrgUnit(TextBox txtPLace, TextBox txtName, ComboBox cbPersonID, List<MongoDBRef> teams)
        {
            OrganizationalUnit ou = new OrganizationalUnit();
            if (cbPersonID.SelectedItem != null)
            {
                ObjectId employeeId = (cbPersonID.SelectedItem as Employee).Id;
                ou.LeaderOfDepartmant = new MongoDBRef("employee_id", employeeId);
            }
            else
            {
            }
            ou.Place = txtPLace.Text;
            ou.OrganizationUnitName = txtName.Text;
            ou.Team = teams;

            ous.Create(ou);
        }

        public void editOrgUnit(ObjectId ID, TextBox txtPLace, TextBox txtName, ComboBox cbPersonID, List<MongoDBRef> teams)
        {
            OrganizationalUnit ou = new OrganizationalUnit();
            ou.Id = ID;
            ObjectId employeeId = (cbPersonID.SelectedItem as Employee).Id;
            ou.LeaderOfDepartmant = new MongoDBRef("employee_id", employeeId);
            ou.Place = txtPLace.Text;
            ou.OrganizationUnitName = txtName.Text;
            ou.Team = teams;

            ous.Update(ou);
        }

        public void removeOrgUnit(OrganizationalUnit orgUnit)
        {
            ous.Delete(orgUnit.Id.ToString());
        }

        public void showAllOrgUnit(DataGridView dgv)
        {
            dgv.DataSource = null;
            dgv.DataSource = ous.GetAll();
        }

        public List<OrganizationalUnit> showOrgUnit()
        {
            return ous.GetAll();
        }

        public OrganizationalUnit getUnitById(string id)
        {
            return ous.GetById(id);
        }

        public void getAllTeamMember(DataGridView dgv,  OrganizationalUnit ou)
        {
            List<Employee> teamMember = new List<Employee>();
            List<Employee> employee = es.GetAll();
            foreach (var item in employee)
            {
                if (ou.Id == item.WorkInDepartmant.Id && item.Id!= ou.LeaderOfDepartmant.Id)
                {
                    teamMember.Add(item);
                }
                if (ou.Team.Count > 0)
                {
                    foreach (var lead in ou.Team)
                    {
                        if (item.WorkInDepartmant.Id == lead.Id )
                        {
                            if (item.LeaderOfDepartmant!="" || item.LeaderOfDepartmant!=string.Empty)
                                teamMember.Add(item);
                        }
                    }
                }
            }
            dgv.DataSource = null;
            dgv.DataSource = teamMember;
        }

        public List<OrganizationalUnit> showTeams()
        {
            List<OrganizationalUnit> allOrgUnit = ous.GetAll();
            List<OrganizationalUnit> teams = new List<OrganizationalUnit>();
            string teamName = "";

            if (allOrgUnit.Count != 0)
            {
                foreach (OrganizationalUnit item in allOrgUnit)
                {
                    if (item.OrganizationUnitName != teamName)
                    {
                        teamName = item.OrganizationUnitName;
                        teams.Add(item);
                    }
                }
            }
            return teams.Distinct().ToList();
        }

        #endregion Organization Unit
    }
}