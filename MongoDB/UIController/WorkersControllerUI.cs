﻿using MongoDB.Bson;
using MongoDB.DataModel;
using MongoDB.Driver;
using MongoDB.Service;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MongoDB.UIController
{
    internal class WorkersControllerUI
    {
        #region Employee

        private EmployeeService es = new EmployeeService();
        private OrganizationUnitService ous = new OrganizationUnitService();

        public void addNewEmployee(TextBox txtFirstName, TextBox txtLastName, TextBox txtPersonID, ComboBox cmbWorker)
        {
            List<Payment> list = new List<Payment>();
            Employee employee = new Employee();
            employee.FirstName = txtFirstName.Text;
            employee.LastName = txtLastName.Text;
            employee.PersonID = txtPersonID.Text;
            ObjectId orgUnitId = (cmbWorker.SelectedItem as OrganizationalUnit).Id;
            employee.WorkInDepartmant = new MongoDBRef("organization_unit_id", orgUnitId);
            employee.ListOfPayment = list;

            es.Create(employee);
        }

        public void removeEmployee(Employee employee)
        {
            es.Delete(employee.Id.ToString());
        }

        public void editEmployee(ObjectId ID, TextBox txtFirstName, TextBox txtLastName, TextBox txtPersonID, ComboBox cmbWorker)
        {
           
            Employee employee = new Employee();
            employee.Id = ID;
            employee.FirstName = txtFirstName.Text;
            employee.LastName = txtLastName.Text;
            employee.PersonID = txtPersonID.Text;
            ObjectId orgUnitId = (cmbWorker.SelectedItem as OrganizationalUnit).Id;
            employee.WorkInDepartmant = new MongoDBRef("organization_unit_id", orgUnitId);
            Employee payout = es.GetById(ID.ToString());
            employee.ListOfPayment = payout.ListOfPayment;

            es.Update(employee);
        }

        public void showAllEmployee(DataGridView dgv)
        {
            dgv.DataSource = null;
            dgv.DataSource = es.GetAll();
        }

        public OrganizationalUnit getOrgUnitById(string id)
        {
            return  ous.GetById(id);
        }

        public Employee getEmployeeById(string id)
        {
            return es.GetById(id);
        }

        #endregion Employee

        #region Coworker

        private CoworkerService cs = new CoworkerService();

        public void addNewCoworker(TextBox txtFirstName, TextBox txtLastName, TextBox txtPersonID, TextBox txtCompesation, DateTimePicker from, DateTimePicker to)
        {
            Coworker cw = new Coworker();
            cw.FirstName = txtFirstName.Text;
            cw.LastName = txtLastName.Text;
            cw.PersonID = txtPersonID.Text;
            cw.Compensation = Convert.ToDouble(txtCompesation.Text);
            cw.DateFrom = String.Format("{0:s}", from.Value);
            cw.DateTo = String.Format("{0:s}", to.Value);

            cs.Create(cw);
        }

        public void editCoworker(ObjectId ID, TextBox txtFirstName, TextBox txtLastName, TextBox txtPersonID, TextBox txtCompesation, DateTimePicker from, DateTimePicker to)
        {
            Coworker cw = new Coworker();
            cw.Id = ID;
            cw.FirstName = txtFirstName.Text;
            cw.LastName = txtLastName.Text;
            cw.PersonID = txtPersonID.Text;
            cw.Compensation = Convert.ToDouble(txtCompesation.Text);
            cw.DateFrom = String.Format("{0:s}", from.Value);
            cw.DateTo = String.Format("{0:s}", to.Value);

            cs.Update(cw);
        }

        public void removeCoworker(Coworker coworker)
        {
            cs.Delete(coworker.Id.ToString());
        }

        public void showAllCoworkers(DataGridView dgv)
        {
            dgv.DataSource = null;
            dgv.DataSource = cs.GetAll();
        }

        #endregion Coworker
    }
}