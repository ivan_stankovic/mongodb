﻿using MongoDB.Bson;
using MongoDB.DataModel;
using MongoDB.Driver;
using MongoDB.Helper;
using MongoDB.UIController;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MongoDB
{
    public partial class OrganizationUnitForm : Form
    {
        private ProjectControllerUI pController;
        private ObjectId ID;
        public List<MongoDBRef> teams;

        public OrganizationUnitForm()
        {
            InitializeComponent();
            pController = new ProjectControllerUI();
            pController.showEmployee(cbPerson);

            teams = new List<MongoDBRef>();
            dataGridView1.ContextMenuStrip = cms;
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MyDataGridView_MouseDown);
        }

        private void MyDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridView1.HitTest(e.X, e.Y);
                dataGridView1.ClearSelection();
                dataGridView1.Rows[hti.RowIndex].Selected = true;
            }
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            teams.Clear();

            btnEdit.Enabled = true;
            btnAdd.Enabled = false;
            
            Int32 rowToUpdate = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            OrganizationalUnit ou = new OrganizationalUnit();
            ou = (DataModel.OrganizationalUnit)dataGridView1.Rows[rowToUpdate].DataBoundItem;

            ou = pController.getUnitById(ou.Id.ToString());
            ID = ou.Id;
            txtName.Text = ou.OrganizationUnitName;
            txtPLace.Text = ou.Place;
            if (ou.LeaderOfDepartmant != null)
            {
                Employee employee = pController.getEmployeeById(ou.LeaderOfDepartmant.Id.ToString());
                cbPerson.SelectedIndex = cbPerson.FindStringExact(employee.PersonID);
            }
            else
            {
                cbPerson.SelectedIndex = -1;
            }
            if (ou.Team.Count != 0)
            {
                for (int i = 0; i < ou.Team.Count; i++)
                {
                    teams.Add(ou.Team[i]);
                    OrganizationalUnit t = pController.getUnitById(ou.Team[i].Id.ToString());
                    ListViewItem lvi = new ListViewItem(t.OrganizationUnitName);
                    listView1.BeginUpdate();
                    listView1.Items.Add(lvi);
                    listView1.EndUpdate();
                }
            }

            TeamForm tf = new TeamForm(this);
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cleanFields();
            Int32 rowToDelete = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            OrganizationalUnit ou = new OrganizationalUnit();
            ou = (DataModel.OrganizationalUnit)dataGridView1.Rows[rowToDelete].DataBoundItem;
            pController.removeOrgUnit(ou);

            refreshOrgUnit();
        }

        private void btnPlus_Click(object sender, EventArgs e)
        {
            TeamForm tf = new TeamForm(this);
            tf.ShowDialog();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (Validation.IsStringEmpty(txtName) &&
                Validation.IsStringEmpty(txtPLace))
            {
                pController.addNewOrgUnit(txtPLace, txtName, cbPerson, teams);
                cleanFields();

                refreshOrgUnit();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (Validation.IsStringEmpty(txtName) &&
               Validation.IsStringEmpty(txtPLace))
            {
                pController.editOrgUnit(ID, txtPLace, txtName, cbPerson, teams);
                cleanFields();

                refreshOrgUnit();
            }
        }

        private void cbPerson_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbPerson.SelectedIndex != -1)
            {
                txtPerson.Text = (cbPerson.SelectedItem as Employee).FirstName + " " + (cbPerson.SelectedItem as Employee).LastName;
            }
        }

         public void cleanFields()
        {
            txtPerson.Text = "";
            txtPLace.Text = "";
            txtName.Text = "";
            cbPerson.SelectedIndex = -1;
            listView1.Items.Clear();

            btnEdit.Enabled = false;
            btnAdd.Enabled = true;
        }

       public void refreshOrgUnit()
        {
            pController.showAllOrgUnit(dataGridView1);
        }

        private void OrganizationUnitForm_Load(object sender, EventArgs e)
        {
            refreshOrgUnit();
            cleanFields();
        }

        private void teamMemberToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Int32 rowToDelete = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            OrganizationalUnit ou = new OrganizationalUnit();
            ou = (DataModel.OrganizationalUnit)dataGridView1.Rows[rowToDelete].DataBoundItem;
            TeamMemberForm tmf = new TeamMemberForm(ou);
            tmf.Show();
        }
    
    }
}
