﻿using MongoDB.Bson;
using MongoDB.DataModel;
using MongoDB.DBBroker;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
using System.Linq;

namespace MongoDB.Service
{
    internal class OrganizationUnitService : IEntityService<OrganizationalUnit>
    {
        protected readonly MongoConnectionHandler<OrganizationalUnit> MongoConnectionHandler;

        public OrganizationUnitService()
        {
            MongoConnectionHandler = new MongoConnectionHandler<OrganizationalUnit>();
        }

        ~OrganizationUnitService()
        {
        }

        public OrganizationalUnit GetById(string id)
        {
            var query = Query<OrganizationalUnit>.EQ(e => e.Id,
            new ObjectId(id));
            return this.MongoConnectionHandler.MongoCollection.FindOne(query);
        }

        public List<OrganizationalUnit> GetAll()
        {
            List<OrganizationalUnit> orgUnit = new List<OrganizationalUnit>();
            var list = this.MongoConnectionHandler.MongoCollection.FindAll();

            var v = (from p in list.AsQueryable()
                     select p).ToList();

            foreach (var item in v)
            {
                OrganizationalUnit ou = new OrganizationalUnit();
                ou.Id = item.Id;
                ou.OrganizationUnitName = item.OrganizationUnitName;
                ou.Place = item.Place;
                ou.LeaderOfDepartmant =item.LeaderOfDepartmant;
                List<MongoDBRef> team = new List<MongoDBRef>();
                if (item.Team != null)
                {
                    foreach (var i in item.Team)
                    {
                        team.Add(i);
                    }
                }
                ou.Team = team;
                orgUnit.Add(ou);
            }

            return orgUnit;
        }

        public void Create(OrganizationalUnit entity)
        {
            var result = this.MongoConnectionHandler.MongoCollection.Insert(entity);
        }

        public void Delete(string id)
        {
            var query = Query<OrganizationalUnit>.EQ(e => e.Id,
               new ObjectId(id));
            this.MongoConnectionHandler.MongoCollection.Remove(query);
        }

        public void Update(OrganizationalUnit entity)
        {
            var query = Query<OrganizationalUnit>.EQ(e => e.Id,
               new ObjectId(entity.Id.ToString()));
            this.MongoConnectionHandler.MongoCollection.Update(query, Update<OrganizationalUnit>.Replace(entity));
        }
    }
}