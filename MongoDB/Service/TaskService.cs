﻿using MongoDB.Bson;
using MongoDB.DataModel;
using MongoDB.DBBroker;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
using System.Linq;

namespace MongoDB.Service
{
    internal class TaskService : IEntityService<MongoDB.DataModel.Task>
    {
        protected readonly MongoConnectionHandler<MongoDB.DataModel.Task> MongoConnectionHandler;

        public TaskService()
        {
            MongoConnectionHandler = new MongoConnectionHandler<MongoDB.DataModel.Task>();

        }

        ~TaskService()
        {
        }

        public DataModel.Task GetById(string id)
        {
            var query = Query<MongoDB.DataModel.Task>.EQ(e => e.Id,
          new ObjectId(id));
            return this.MongoConnectionHandler.MongoCollection.FindOne(query);
        }

        public List<DataModel.Task> GetAll()
        {
            List<DataModel.Task> task = new List<DataModel.Task>();
            var list = this.MongoConnectionHandler.MongoCollection.FindAll();

            var v = (from p in list.AsQueryable()
                     select p).ToList();

            foreach (var item in v)
            {
                DataModel.Task t = new DataModel.Task();
                t.Id = item.Id;
                t.TaskName = item.TaskName;
                t.TaskDate = item.TaskDate;
                t.PlannedEndDate = item.PlannedEndDate;
                t.ActualEndDate = item.ActualEndDate;
                t.Project = item.Project;
                t.TypesOfTask = item.TypesOfTask;
                List<MongoDBRef> employee = new List<MongoDBRef>();
                if (item.Participants != null)
                {
                    foreach (var i in item.Participants)
                    {
                        employee.Add(i);
                    }
                }
                t.Participants = employee;
                task.Add(t);
            }

            return task;
        }

        public void Create(DataModel.Task entity)
        {
            var owner = this.MongoConnectionHandler.MongoCollection.Database.FetchDBRefAs<Project>(entity.Project);
            var result = this.MongoConnectionHandler.MongoCollection.Insert(entity);
        }

        public void Delete(string id)
        {
            var query = Query<MongoDB.DataModel.Task>.EQ(e => e.Id,
              new ObjectId(id));
            this.MongoConnectionHandler.MongoCollection.Remove(query);
        }

        public void Update(DataModel.Task entity)
        {
            var query = Query<MongoDB.DataModel.Task>.EQ(e => e.Id,
              new ObjectId(entity.Id.ToString()));
            this.MongoConnectionHandler.MongoCollection.Update(query, Update<MongoDB.DataModel.Task>.Replace(entity));
        }
    }
}