﻿using MongoDB.Bson;
using MongoDB.DataModel;
using MongoDB.DBBroker;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
using System.Linq;

namespace MongoDB.Service
{
    internal class CoworkerService : IEntityService<Coworker>
    {
        protected readonly MongoConnectionHandler<Coworker> MongoConnectionHandler;

        public CoworkerService()
        {
            MongoConnectionHandler = new MongoConnectionHandler<Coworker>();
        }

        ~CoworkerService()
        {
        }

        public Coworker GetById(string id)
        {
            return new Coworker();
        }

        public List<Coworker> GetAll()
        {
            List<Coworker> coworker = new List<Coworker>();
            var list = this.MongoConnectionHandler.MongoCollection.FindAll();

            var v = (from p in list.AsQueryable()
                     select p).ToList();

            foreach (var item in v)
            {
                Coworker cw = new Coworker();
                cw.Id = item.Id;
                cw.PersonID = item.PersonID;
                cw.Compensation = item.Compensation;

                cw.DateFrom = item.DateFrom;
                cw.DateTo = item.DateTo;
                cw.FirstName = item.FirstName;
                cw.LastName = item.LastName;

                coworker.Add(cw);
            }

            return coworker;
        }

        public void Create(Coworker entity)
        {
            var result = this.MongoConnectionHandler.MongoCollection.Insert(entity);
        }

        public void Delete(string id)
        {
            var query = Query<Coworker>.EQ(e => e.Id,
               new ObjectId(id));
            this.MongoConnectionHandler.MongoCollection.Remove(query);
        }

        public void Update(Coworker entity)
        {
            var query = Query<Coworker>.EQ(e => e.Id,
                new ObjectId(entity.Id.ToString()));
            this.MongoConnectionHandler.MongoCollection.Update(query, Update<Coworker>.Replace(entity));
        }
    }
}