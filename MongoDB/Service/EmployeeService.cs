﻿using MongoDB.Bson;
using MongoDB.DataModel;
using MongoDB.DBBroker;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
using System.Linq;

namespace MongoDB.Service
{
    internal class EmployeeService : IEntityService<Employee>
    {
        protected readonly MongoConnectionHandler<Employee> MongoConnectionHandler;

        public EmployeeService()
        {
            MongoConnectionHandler = new MongoConnectionHandler<Employee>();
        }

        ~EmployeeService()
        {
        }

        public Employee GetById(string id)
        {
            var query = Query<Employee>.EQ(e => e.Id,
            new ObjectId(id));
            return this.MongoConnectionHandler.MongoCollection.FindOne(query);
        }

        public List<Employee> GetAll()
        {
            List<Employee> employee = new List<Employee>();
            var list = this.MongoConnectionHandler.MongoCollection.FindAll();

            var v = (from p in list.AsQueryable()
                     select p).ToList();

            foreach (var item in v)
            {
                Employee e = new Employee();
                e.Id = item.Id;
                e.PersonID = item.PersonID;
                e.WorkInDepartmant = item.WorkInDepartmant;
                e.FirstName = item.FirstName;
                e.LastName = item.LastName;

                employee.Add(e);
            }

            return employee;
        }

        public void Create(Employee entity)
        {
            var result = this.MongoConnectionHandler.MongoCollection.Insert(entity);
        }

        public void Delete(string id)
        {
            var query = Query<Employee>.EQ(e => e.Id,
              new ObjectId(id));
            this.MongoConnectionHandler.MongoCollection.Remove(query);
        }

        public void Update(Employee entity)
        {
            var query = Query<Employee>.EQ(e => e.Id,
                new ObjectId(entity.Id.ToString()));
            this.MongoConnectionHandler.MongoCollection.Update(query, Update<Employee>.Replace(entity));
        }
    }
}