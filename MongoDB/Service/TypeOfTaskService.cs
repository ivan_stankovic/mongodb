﻿using MongoDB.Bson;
using MongoDB.DataModel;
using MongoDB.DBBroker;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
using System.Linq;

namespace MongoDB.Service
{
    internal class TypeOfTaskService : IEntityService<TypeOfTasks>
    {
        protected readonly MongoConnectionHandler<TypeOfTasks> MongoConnectionHandler;

        public TypeOfTaskService()
        {
            MongoConnectionHandler = new MongoConnectionHandler<TypeOfTasks>();
        }

        ~TypeOfTaskService()
        {
        }

        public TypeOfTasks GetById(string id)
        {
            var query = Query<TypeOfTasks>.EQ(e => e.Id,
                new ObjectId(id));
            return this.MongoConnectionHandler.MongoCollection.FindOne(query);
        }

        public List<TypeOfTasks> GetAll()
        {
            List<TypeOfTasks> types = new List<TypeOfTasks>();
            var list = this.MongoConnectionHandler.MongoCollection.FindAll();

            var v = (from p in list.AsQueryable()
                     select p).ToList();

            foreach (var item in v)
            {
                TypeOfTasks tot = new TypeOfTasks();
                tot.Id = item.Id;
                tot.TypeName = item.TypeName;
                types.Add(tot);
            }

            return types;
        }

        public void Create(TypeOfTasks entity)
        {
            var result = this.MongoConnectionHandler.MongoCollection.Insert(entity);
        }

        public void Delete(string id)
        {
            var query = Query<TypeOfTasks>.EQ(e => e.Id,
              new ObjectId(id));
            this.MongoConnectionHandler.MongoCollection.Remove(query);
        }

        public void Update(TypeOfTasks entity)
        {
        }
    }
}