﻿using MongoDB.DataModel;
using System.Collections.Generic;

namespace MongoDB.Service
{
    internal interface IEntityService<T> where T : IMongoEntity
    {
        T GetById(string id);

        List<T> GetAll();

        void Create(T entity);

        void Delete(string id);

        void Update(T entity);
    }
}