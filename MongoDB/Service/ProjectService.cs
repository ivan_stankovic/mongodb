﻿using MongoDB.Bson;
using MongoDB.DataModel;
using MongoDB.DBBroker;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
using System.Linq;

namespace MongoDB.Service
{
    internal class ProjectService : IEntityService<Project>
    {
        protected readonly MongoConnectionHandler<Project> MongoConnectionHandler;

        public ProjectService()
        {
            MongoConnectionHandler = new MongoConnectionHandler<Project>();
        }

        ~ProjectService()
        {
        }

        public List<Project> GetAll()
        {
            List<Project> project = new List<Project>();
            var list = this.MongoConnectionHandler.MongoCollection.FindAll();

            var v = (from p in list.AsQueryable()
                     select p).ToList();

            foreach (var item in v)
            {
                Project p = new Project();
                p.Id = item.Id;
                p.ProjectName = item.ProjectName;
                project.Add(p);
            }

            return project;
        }

        public void Create(Project entity)
        {
            var result = this.MongoConnectionHandler.MongoCollection.Insert(entity);
        }

        public void Delete(string id)
        {
            var query = Query<Project>.EQ(e => e.Id,
                new ObjectId(id));
            this.MongoConnectionHandler.MongoCollection.Remove(query);
        }

        public Project GetById(string id)
        {
            var query = Query<Project>.EQ(e => e.Id,
         new ObjectId(id));
            return this.MongoConnectionHandler.MongoCollection.FindOne(query);
        }

        public void Update(Project entity)
        {
        }
    }
}