﻿using MongoDB.Bson;
using MongoDB.DataModel;
using MongoDB.DBBroker;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
using System.Linq;

namespace MongoDB.Service
{
    internal class PaymentBasisService : IEntityService<PaymentBasis>
    {
        protected readonly MongoConnectionHandler<PaymentBasis> MongoConnectionHandler;

        public PaymentBasisService()
        {
            MongoConnectionHandler = new MongoConnectionHandler<PaymentBasis>();
        }

        ~PaymentBasisService()
        {
        }

        public PaymentBasis GetById(string id)
        {
            return new PaymentBasis();
        }

        public List<PaymentBasis> GetAll()
        {
            List<PaymentBasis> basis = new List<PaymentBasis>();
            var list = this.MongoConnectionHandler.MongoCollection.FindAll();

            var v = (from p in list.AsQueryable()
                     select p).ToList();

            foreach (var item in v)
            {
                PaymentBasis pb = new PaymentBasis();
                pb.Id = item.Id;
                pb.Basis = item.Basis;
                basis.Add(pb);
            }

            return basis;
        }

        public void Create(PaymentBasis entity)
        {
            var result = this.MongoConnectionHandler.MongoCollection.Insert(entity);
        }

        public void Delete(string id)
        {
            var query = Query<PaymentBasis>.EQ(e => e.Id,
               new ObjectId(id));
            this.MongoConnectionHandler.MongoCollection.Remove(query);
        }

        public void Update(PaymentBasis entity)
        {
        }
    }
}