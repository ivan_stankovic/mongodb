﻿using MongoDB.Bson;
using MongoDB.DataModel;
using MongoDB.Driver;
using MongoDB.UIController;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace MongoDB
{
    public partial class TeamForm : Form
    {
        private ProjectControllerUI pController;

        private CheckBox cb;
        private string name = "cb_";
        private List<MongoDBRef> list;
        private OrganizationUnitForm ouf;

        public TeamForm(OrganizationUnitForm _ouf)
        {
            InitializeComponent();
            pController = new ProjectControllerUI();
            ouf = _ouf;
            list = ouf.teams;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ouf.listView1.Items.Clear();
            List<string> teams = new List<string>();
            foreach (Control c in this.Controls)
            {
                if (c is CheckBox)
                {
                    CheckBox cb = (CheckBox)c;
                    if (cb.CheckState == CheckState.Checked)
                    {
                        teams.Add(cb.Text);
                    }
                }
            }

            List<OrganizationalUnit> teamInOrgUnit = pController.showOrgUnit();
            List<MongoDBRef> ownedTeams = new List<MongoDBRef>();

            if (teams.Count != 0)
            {
                for (int i = 0; i < teams.Count; i++)
                {
                    ListViewItem lvi = new ListViewItem(teams[i]);
                    ouf.listView1.BeginUpdate();
                    ouf.listView1.Items.Add(lvi);
                    ouf.listView1.EndUpdate();

                    foreach (var item in teamInOrgUnit)
                    {
                        if (teams[i] == item.OrganizationUnitName)
                        {
                            ObjectId orgUnitId = item.Id;
                            var orgUnitRef = new MongoDBRef("team_id", orgUnitId);
                            ownedTeams.Add(orgUnitRef);
                        }
                    }
                }
            }
            ouf.teams = ownedTeams;
            this.Close();
        }

        private void TeamForm_Load(object sender, EventArgs e)
        {
            int y = 10;
            List<OrganizationalUnit> teams = pController.showTeams();
            for (int n = 0; n < teams.Count; n++)
            {
                cb = new CheckBox();
                cb.Location = new Point(10, y);
                cb.Name = name + n.ToString();
                cb.Text = teams[n].ToString();

                Controls.Add(cb);
                y += 25;
            }

            foreach (Control c in this.Controls)
            {
                if (c is CheckBox)
                {
                    CheckBox cb = (CheckBox)c;
                    for (int i = 0; i < list.Count; i++)
                    {
                        OrganizationalUnit ou = pController.getUnitById(list[i].Id.ToString());
                        if (cb.Text == ou.OrganizationUnitName)
                        {
                            cb.CheckState = CheckState.Checked;
                        }
                    }
                }
            }
        }
    }
}