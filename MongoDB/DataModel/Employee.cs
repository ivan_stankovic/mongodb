﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Service;
using System.Collections.Generic;
using System.ComponentModel;

namespace MongoDB.DataModel
{
    public class Employee : Person, IMongoEntity
    {
        [BsonId]
        private ObjectId _id;

        private MongoDBRef _workInDepartmant;
        private List<Payment> _listOfPayment;

       

        public Employee()
        {
        }

        public Employee(ObjectId employeeID, string personID, string firstName, string lastName, MongoDBRef workInDepartmant)
            : base(personID, firstName, lastName)
        {
            _id = employeeID;
            _workInDepartmant = workInDepartmant;
        }

        [Browsable(false)]
        public Bson.ObjectId Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public List<Payment> ListOfPayment
        {
            get { return _listOfPayment; }
            set { _listOfPayment = value; }
        }

        
        [Browsable(false)]
        public MongoDBRef WorkInDepartmant
        {
            get { return _workInDepartmant; }
            set { _workInDepartmant = value; }
        }

        private OrganizationUnitService ous = new OrganizationUnitService();
        [DisplayName("Work in department")]
        public OrganizationalUnit WorkInDepartmantName
        {
            get
            {
                if (_workInDepartmant != null)
                {
                    return ous.GetById(_workInDepartmant.Id.ToString());
                }
                return new OrganizationalUnit();
            }
        }

        [DisplayName("Leader of department")]
        [Browsable(true)]
        public string LeaderOfDepartmant
        {
            get
            {
                List<OrganizationalUnit> list = ous.GetAll();

                foreach (var item in list)
                {
                    if (item.LeaderOfDepartmant != null)
                    {
                        if (Id == item.LeaderOfDepartmant.Id)
                        {
                            OrganizationalUnit ou = ous.GetById(item.LeaderOfDepartmant.Id.ToString());
                            return item.OrganizationUnitName;
                        }
                    }
                }
                return "";
            }
        }

        public override string ToString()
        {
            return base.PersonID.ToString();
        }
    }
}