﻿using System.ComponentModel;

namespace MongoDB.DataModel
{
    internal class TypeOfTasks : MongoEntity
    {
        private string _typeName;

        public TypeOfTasks()
        {
        }

        public TypeOfTasks(string typeName)
        {
            _typeName = typeName;
        }

        [DisplayName("Type of task")]
        [Browsable(true)]
        public string TypeName
        {
            get { return _typeName; }
            set { _typeName = value; }
        }

        public override string ToString()
        {
            return TypeName.ToString();
        }
    }
}