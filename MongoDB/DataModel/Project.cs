﻿using MongoDB.Bson;
using System.ComponentModel;

namespace MongoDB.DataModel
{
    internal class Project : MongoEntity
    {
        private string _projectName;

        public Project()
        {
        }

        public Project(ObjectId id, string projectName)
            : base(id)
        {
            _projectName = projectName;
        }

        [DisplayName("Name")]
        [Browsable(true)]
        public string ProjectName
        {
            get { return _projectName; }
            set { _projectName = value; }
        }

        public override string ToString()
        {
            return ProjectName.ToString();
        }
    }
}