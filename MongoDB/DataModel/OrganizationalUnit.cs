﻿using MongoDB.Driver;
using MongoDB.Service;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MongoDB.DataModel
{
    public class OrganizationalUnit : MongoEntity
    {
        private string _organizationUnitName;
        private MongoDBRef _leaderOfDepartmant;
        private string _place;
        private List<MongoDBRef> _team;

        public OrganizationalUnit()
        {
        }

        public OrganizationalUnit(string organizationUnitName, MongoDBRef leaderOfDepartmant, string place, List<MongoDBRef> team)
        {
            _organizationUnitName = organizationUnitName;
            _leaderOfDepartmant = leaderOfDepartmant;
            _place = place;
            _team = team;
        }

        [DisplayName("Department name")]
        public string OrganizationUnitName
        {
            get { return _organizationUnitName; }
            set { _organizationUnitName = value; }
        }

        [Browsable(false)]
        public MongoDBRef LeaderOfDepartmant
        {
            get { return _leaderOfDepartmant; }
            set { _leaderOfDepartmant = value; }
        }
        private EmployeeService es = new EmployeeService();
        [DisplayName("Leader of department")]
        public string EmployeeName
        {
            get
            {
                if (_leaderOfDepartmant != null)
                {
                    Employee employee = es.GetById(_leaderOfDepartmant.Id.ToString());
                    return employee.FirstName + " " + employee.LastName;
                }
                return "";
            }
        }

        [DisplayName("Place")]
        public string Place
        {
            get { return _place; }
            set { _place = value; }
        }

        [Browsable(false)]
        public List<MongoDBRef> Team
        {
            get { return _team; }
            set { _team = value; }
        }

        private OrganizationUnitService ous = new OrganizationUnitService();
        [DisplayName("Teams")]
        public string Teams
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                if (_team.Count != 0)
                {
                    for (int i = 0; i < _team.Count; i++)
                    {
                        OrganizationalUnit ou = ous.GetById(_team[i].Id.ToString());
                        sb.Append(ou.OrganizationUnitName);
                        if (i < _team.Count - 1)
                            sb.Append(",");
                    }
                }
                return sb.ToString();
            }
        }

        public override string ToString()
        {
            return OrganizationUnitName.ToString();
        }
    }
}