﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel;

namespace MongoDB.DataModel
{
    public class Payment
    {
        [BsonId]
        [Browsable(false)]
        public ObjectId Id { get; set; }
        private string _paymentDate;
        private double _payout;
        private PaymentBasis _paymentBasis;

        public Payment()
        {
        }

        public Payment( string paymentDate, double payout, PaymentBasis paymentBasis)
        {
            _paymentDate = paymentDate;
            _payout = payout;
            _paymentBasis = paymentBasis;
        }

      

        [DisplayName("Payout date")]
        public string PaymentDate
        {
            get
            {
                DateTime enteredDate = DateTime.Parse(_paymentDate);
                return enteredDate.ToString("MM/dd/yyyy");
            }
            set { _paymentDate = value; }
        }

        [DisplayName("Payout")]
        public double Payout
        {
            get { return _payout; }
            set { _payout = value; }
        }

        [DisplayName("Payment basis")]
        public PaymentBasis PaymentBasis
        {
            get { return _paymentBasis; }
            set { _paymentBasis = value; }
        }
    }
}