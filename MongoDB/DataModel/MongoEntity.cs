﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel;

namespace MongoDB.DataModel
{
    public class MongoEntity : IMongoEntity
    {
        [BsonId]
        [Browsable(false)]
        public ObjectId Id { get; set; }

        public MongoEntity()
        {
        }

        public MongoEntity(ObjectId id)
        {
            this.Id = id;
        }
    }
}