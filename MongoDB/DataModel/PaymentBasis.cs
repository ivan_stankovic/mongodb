﻿using System.ComponentModel;

namespace MongoDB.DataModel
{
    public class PaymentBasis : MongoEntity
    {
        private double _basis;

        public PaymentBasis()
        {
        }

        public PaymentBasis(double basis)
        {
            _basis = basis;
        }

        [DisplayName("Basis payment")]
        [Browsable(true)]
        public double Basis
        {
            get { return _basis; }
            set { _basis = value; }
        }

        public override string ToString()
        {
            return _basis + "";
        }
    }
}