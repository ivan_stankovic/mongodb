﻿using MongoDB.Driver;
using MongoDB.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MongoDB.DataModel
{
    internal class Task : MongoEntity
    {
        private string _taskName;
        private string _taskDate;
        private string _plannedEndDate;
        private string _actualEndDate;
        private MongoDBRef _project;
        private MongoDBRef _typesOfTask;
        private List<MongoDBRef> _participants;

        public Task()
        {
        }

        public Task(string taskName, string taskDate, string plannedEndDate, string actualEndDate, MongoDBRef project, MongoDBRef typeOfTasks, List<MongoDBRef> participants)
        {
            _taskName = taskName;
            _taskDate = taskDate;
            _plannedEndDate = plannedEndDate;
            _actualEndDate = actualEndDate;
            _project = project;
            _typesOfTask = typeOfTasks;
            _participants = participants;
        }

        [DisplayName("Task name")]
        [Browsable(true)]
        public string TaskName
        {
            get { return _taskName; }
            set { _taskName = value; }
        }


        [Browsable(false)]
        public MongoDBRef Project
        {
            get { return _project; }
            set { _project = value; }
        }

        private ProjectService ps = new ProjectService();
        [DisplayName("Task belong to project")]
        public Project ProjectName
        {
            get
            {
                return ps.GetById(_project.Id.ToString());
            }
        }


        [Browsable(false)]
        public MongoDBRef TypesOfTask
        {
            get { return _typesOfTask; }
            set { _typesOfTask = value; }
        }

        //[DisplayName("Department")]
        //public string Department
        //{
        //    get
        //    {
        //        if (_participants.Count != 0)
        //        {
        //            foreach (var item in _participants)
        //            {
        //                if (item.LeaderOfDepartmant != string.Empty || item.LeaderOfDepartmant != "" || item.LeaderOfDepartmant != " ")
        //                {
        //                    return item.LeaderOfDepartmant;
        //                }
        //            }
        //        }
        //        return "";
        //    }
        //}

        private TypeOfTaskService tots = new TypeOfTaskService();
        [DisplayName("Type of task")]
        public TypeOfTasks TypeName
        {
            get
            {
                return tots.GetById(_typesOfTask.Id.ToString());
            }
        }


        [DisplayName("Start task date")]
        public string TaskDate
        {
            get
            {
                DateTime enteredDate = DateTime.Parse(_taskDate);
                return enteredDate.ToString("MM/dd/yyyy");
            }
            set { _taskDate = value; }
        }

        [DisplayName("Planned end date of task")]
        public string PlannedEndDate
        {
            get
            {
                DateTime enteredDate = DateTime.Parse(_plannedEndDate);
                return enteredDate.ToString("MM/dd/yyyy");
            }
            set { _plannedEndDate = value; }
        }

        [DisplayName("Actual end date of task")]
        public string ActualEndDate
        {
            get
            {
                DateTime enteredDate = DateTime.Parse(_actualEndDate);
                return enteredDate.ToString("MM/dd/yyyy");
            }
            set { _actualEndDate = value; }
        }

        [DisplayName("Participant")]
        [Browsable(false)]
        public List<MongoDBRef> Participants
        {
            get { return _participants; }
            set { _participants = value; }
        }
    }
}