﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel;

namespace MongoDB.DataModel
{
    internal class Coworker : Person, IMongoEntity
    {
        [BsonId]
        private ObjectId _id;

        private string _dateFrom;
        private string _dateTo;
        private double _compensation;

        public Coworker()
        {
        }

        public Coworker(ObjectId id, string personID, string firstName, string lastName, string dateFrom, string dateTo, double compensation)
            : base(personID, firstName, lastName)
        {
            _id = id;
            _dateFrom = dateFrom;
            _dateTo = dateTo;
            _compensation = compensation;
        }

        [Browsable(false)]
        public ObjectId Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [DisplayName("Date from")]
        public string DateFrom
        {
            get
            {
                DateTime enteredDate = DateTime.Parse(_dateFrom);
                return enteredDate.ToString("MM/dd/yyyy");
            }
            set { _dateFrom = value; }
        }

        [DisplayName("Date to")]
        public string DateTo
        {
            get
            {
                DateTime enteredDate = DateTime.Parse(_dateTo);
                return enteredDate.ToString("MM/dd/yyyy");
            }
            set { _dateTo = value; }
        }

        [DisplayName("Compensation")]
        public double Compensation
        {
            get { return _compensation; }
            set { _compensation = value; }
        }
    }
}