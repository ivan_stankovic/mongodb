﻿using MongoDB.Bson;

namespace MongoDB.DataModel
{
    internal interface IMongoEntity
    {
        ObjectId Id { get; set; }
    }
}