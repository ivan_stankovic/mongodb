﻿using MongoDB.DataModel;
using MongoDB.Driver;
using MongoDB.UIController;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace MongoDB
{
    public partial class TaskMemberForm : Form
    {
        private TaskForm tf;
        private TaskControllerUI tController;
        private List<MongoDBRef> person;

        public TaskMemberForm(TaskForm _tf)
        {
            InitializeComponent();
            person = new List<MongoDBRef>();
            tController = new TaskControllerUI();
            tf = _tf;

            person = tf.person;

            List<Employee> emlpoyee = new List<Employee>();
            foreach (var item in person)
            {
                Employee e = tController.getEmployeeById(item.Id.ToString());
                emlpoyee.Add(e);
            }

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = emlpoyee;
           // AdjustColumnOrder();
        }

        private void AdjustColumnOrder()
        {
            dataGridView1.Columns["PersonID"].DisplayIndex = 0;
            dataGridView1.Columns["FirstName"].DisplayIndex = 1;
            dataGridView1.Columns["LastNAme"].DisplayIndex = 2;
            dataGridView1.Columns["WorkInDepartmant"].DisplayIndex = 3;
            dataGridView1.Columns["LeaderOfDepartmant"].DisplayIndex = 4;
        }
    }
}